# Formation API

Training API & REST architecture.

Cette formation est à destination d'étudiants d'enseignement supérieur, elle n'a pas vocation à couvrir l'ensemble de l'état de l'art sur ces sujets.

À l'issue de cette formation, les élèves devraient:

- **Acquérir une compréhension** du concept d'API et de certaines de ses implémentations en fonction des cas d'applications.
- **Acquérir une compréhension** des standards du web necessaire à la mise en oeuvre d'API HTTP
- **Savoir appeler** une API en utilisant Postman
- **Acquérir une compéhension** de l'architecture REST et des API RESTful
- **Savoir designer** et **développer** des APIs RESTful
- **Acquérir une compréhension** des problématiques rencontrés dans les architectures distribuées tel que les microservices ainsi que de frameworks permettant d'y palier: API Gateway & Service Mesh.
- **Savoir utiliser** une API Gateway pour sécuriser et exposer des API d'un système distribué vers l'extérieur
