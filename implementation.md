---
marp: true
theme: uncover
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

# API Implementation

---

## Always start from users needs

**As a** doctor Thomas  
**I want to** review all my patients medical history  
**So that** i provide the best treatment  

--- 

Let's imagine we did some UI/UX research with our users, and they want:
- To search patients by one or several criterias:
  - social security number, last name or date of birth

- The filtered patients should display the fullname, date for birth, gender and social security number.

- When a given patient is selected, all his informations should be displayed, including the medical history.

---

## Design the API Spec

We'll start by designing our API, this way the developpment of the Frontend and Backend can be done simultaneously.
It also allows us to discuss and refine our understanding of the business domain.
If during implementation, it needs to be changed, it is possible but it needs to be discussed with every one involved.

---

## Let's implement the API in our backend

Pre-requisite:

- An IDE of your choice ([Intellij](https://www.jetbrains.com/idea/), [Eclipse](https://www.eclipse.org/), [VSCode](https://code.visualstudio.com/), ...)
- [Apache Maven 3.8.6](https://maven.apache.org/download.cgi)
- [JDK 19 installed with JAVA_HOME configured appropriately](https://jdk.java.net/java-se-ri/19)

---

## Let's bootstrap a [quarkus](https://quarkus.io/) application

![bg left:30% 80%](assets/quarkus_logo.png)

- A Kubernetes Native Java stack
- Out of the box native compilation
- Support reactive style
- Much more ...
- Opensource & Maintained by RedHat


Go to https://code.quarkus.io

---

![center 80% 80%](assets/quarkus_new_app.png)

- groupId – a unique base name of the company or group that created the project
- artifactId – a unique name of the project
- Add dependencies: 
  - `quarkus-mongodb-panache`
  - `quarkus-resteasy`
  - `quarkus-resteasy-jackson`

---

- Download as Zip
- Go to to git repository we used previsouly
- Create a new git branch `git checkout -b implement-rest-api`
- Unzip the archive in the Git repository
- Open the directory in the IDE of your choice

---

Compile and start the application in dev mode:

`./mvnw compile quarkus:dev`

You should be able to naviguate to http://localhost:8080/q/dev/

---

# Test first / TDD

Always start by writing a test describing how the part of the application you are trying to implement should behave.

Then implement it.

Then you can refactor if needed.

And repeat.

---

![tdd cycle](./assets/tdd.png)

---

- We will add the `rest-assured` library to help with our testing by adding the following snippet in our pom.xml
```
<dependency>
    <groupId>io.rest-assured</groupId>
    <artifactId>rest-assured</artifactId>
    <version>4.5.1</version>
    <scope>test</scope>
</dependency>
```

---

- Create a src/test/java directory
- Create a package with your `groupId` semantic (i.e. `com.isen.groncajolo`)
- Create a java class `PatientResourceTest`
- Add the annotation @QuarkusTest to the class
- Create a test, give it a name that explains what is tested

---

We will start by testing that the `/patients` endpoint when called with the `GET` method and a header `Content-Type: application/json` returns a `200` status code and a correct body.

<!---
    @Test
    public void shouldReturnPatients() {
        given()
                .header("Content-Type", "application/json")
                .when().get("/patients")
                .then()
                .statusCode(200)
                .body("[0].name", equalTo("John Doe"))
                .body("[0].sex", equalTo("M"))
                .body("[0].socialSecurityNumber", equalTo(Long.valueOf("123456789012345")));
    }
--->

---

When we run the test, it fails as expected because the endpoint returns a `404`.

Now, we can start implementing it until the test passes.

---

- In src/main/java, create a mirror package to the one created in the test folder.
- Create a `PatientResource` java class
- Add a `@Path("/patients")` annotation
- Create a getPatients method, that returns as a string the expected JSON. Add the `@GET` and `@Produces(MediaType.APPLICATION_JSON)` annotations

<!---
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/patients")
public class PatientResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getPatients() {
        return "[{ \"id\": 1, \"name\": \"John Doe\", \"sex\": \"M\", \"socialSecurityNumber\": 123456789012345}]";
    }
}
--->

---

Our test now passes.
But we only returned a string instead of returning data.
We can keep going and implement our entity and repository.

---

- Create a `PatientEntity` java class, add the `@MongoEntity(collection="patients")` annotation to the class, declare the `Patient` attributes and add the `@JsonProperty` annotations to each of them (except id).

- Create a `PatientRepository` java class, make it implement `PanacheMongoRepository<PatientEntity>` and add the `@ApplicationScoped` annotation

<!---
@MongoEntity(collection="patients")
public class PatientEntity {

    public ObjectId id;

    @JsonProperty
    public String name;

    @JsonProperty
    public String sex;

    @JsonProperty
    public Long socialSecurityNumber;

}

@ApplicationScoped
public class PatientRepository implements PanacheMongoRepository<PatientEntity> {
}

--->

---

- Now we can make our getPatients return a List of `PatientEntity` and retrive them from our mongodb.
  - Inject our PatientRepository
  - Return the list of patients from our mongodb using the PatientRespositoy

<!---
@Path("/patients")
public class PatientResource {

    @Inject
    PatientRepository patientRepository;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<PatientEntity> getPatients() {
        return patientRepository.findAll().list();
    }
}
--->

---


The test fails with a 500 error, we need to declare a mongodb database.

Add `quarkus.mongodb.database=patient-ms` to our `application.properties`.

Now our test fails because the endpoint returned null.

We can have our test inject data in the mongodb

<!---
@QuarkusTest
public class PatientResourceTest {

    @Inject
    PatientRepository patientRepository;

    @Test
    public void shouldReturnPatients() {
        patientRepository.persist(new PatientEntity("John Doe", "M", Long.valueOf("123456789012345")));
        given()
                .header("Content-Type", "application/json")
                .when().get("/patients")
                .then()
                .statusCode(200)
                .body("[0].name", equalTo("John Doe"))
                .body("[0].sex", equalTo("M"))
                .body("[0].socialSecurityNumber", equalTo(Long.valueOf("123456789012345")));
    }
}

Add constructors to PatientEntity:

    public PatientEntity(String name, String sex, Long socialSecurityNumber) {
        this.name = name;
        this.sex = sex;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public PatientEntity() {

    }
--->

---

We can now use the same methodology to implement the other methods / endpoints of our API.

- Patient creation
- Patient update
- Patient Deletion

---

## Congratulation !
you have implemented CRUD on a resource through a RESTful API.

Please commit your work in the same repository we used previously and push it so that I can grade it.

---

## What's next ?

You should take care of testing how your API when improper values are provided.

For example, what happends when you call the `/patients` endpoint using the POST method, but without providing a body ?

Is this the expected behavior ?

---

You might have seen that your server answers with a 500 error and a stacktrace.
As the values provided by the client are not correct, you should return an error `400`.

Add a test case to test this behavior.

---

In quarkus, we can address this by using the following library:

```
<dependency>
  <groupId>io.quarkus</groupId>
  <artifactId>quarkus-hibernate-validator</artifactId>
</dependency>
```

And adding the annotation `@NotNull` to the method argument.

```
public Response insert(@NotNull PatientEntity patient) {...}
```

---

Other validations can be made, for example, a field can be made mandatory using the `@NotBlank(message="Name may not be blank")` on the attribute that is mandatory and the `@Valid` on the request's method argument.