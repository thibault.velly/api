package com.isen.groncajolo;

import io.quarkus.test.junit.QuarkusTest;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
public class PatientResourceTest {

    @Inject
    PatientRepository patientRepository;

    @Test
    public void shouldReturnPatients() {
        patientRepository.persist(new PatientEntity("John Doe" , "M", Long.valueOf("123456789012345")));
        given()
                .header("Content-Type", "application/json")
                .when().get("/patients")
                .then()
                .statusCode(200)
                .body("[0].name", equalTo("John Doe"))
                .body("[0].sex", equalTo("M"))
                .body("[0].socialSecurityNumber", equalTo(Long.valueOf("123456789012345")));
    }

    @Test
    public void shouldRetrunPatient() {
        ObjectId id = new ObjectId();
        patientRepository.persist(new PatientEntity(id, "John Doe" , "M", Long.valueOf("123456789012345")));
        given()
                .header("Content-Type", "application/json")
                .pathParam("id", id.toHexString())
                .when().get("/patients/{id}")
                .then()
                .statusCode(200)
                .body("name", equalTo("John Doe"))
                .body("sex", equalTo("M"))
                .body("socialSecurityNumber", equalTo(Long.valueOf("123456789012345")));
    }
}
